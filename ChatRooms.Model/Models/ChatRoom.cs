﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.Models
{
    public class ChatRoom
    {
        public int ChatRoomId { get; set; }

        public string Name { get; set; }

        public string OwnerName { get; set; }

        public DateTime? LastActivityTime { get; set; }

        public DateTime TimeCreated { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public ChatRoom()
        {
            LastActivityTime = DateTime.Now;
            TimeCreated = DateTime.Now;
        }
    }
}
