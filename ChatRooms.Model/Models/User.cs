﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.Models
{
    public class User
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public bool IsOnline { get; set; }

        public bool IsGuest { get; set; }

        public string ConnectionIdInHub { get; set; }

        public DateTime TimeCreated { get; set; }

        public int? ChatRoomId { get; set; }

        public virtual ChatRoom ChatRoom { get; set; }

        public DateTime? LastActivityTime { get; set; }

        public User()
        {
            TimeCreated = DateTime.Now;
            LastActivityTime = DateTime.Now;
            IsOnline = false;
            ConnectionIdInHub = "";
        }
    }
}
