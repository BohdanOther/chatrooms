﻿using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.Domain.Services
{
    public interface IUserService
    {
        void AddUser(User newUser);
        void DeleteUser(User newUser);
        void EnterRoom(int roomId, string userName);
        void EnterRoom(int roomId, User user);
        void LeaveRoom(string userName);
        void LeaveRoom(User user);
        void DeleteTemporaryUsers(DateTime now, int deltaMinutes);
        void AssignConnection(string username, string connectionId);
        List<User> GetAllUsersInRoom(int roomId);
        User GetUserByName(string name);
        void RegisterUser(User user);
    }

    public class UserService : IUserService
    {
        private IGenericRepository<User> userRepository;

        public UserService(IGenericRepository<User> userRepository)
        {
            this.userRepository = userRepository;
        }

        public List<User> GetAllUsersInRoom(int roomId)
        {
            List<User> users = userRepository
                .GetMany(usr => usr.ChatRoomId == roomId).ToList();

            return users;
        }

        public User GetUserByName(string name)
        {
            User user = userRepository.Get(u => u.Name == name);
            return user;
        }

        public void AddUser(User newUser)
        {
            userRepository.Add(newUser);
        }

        public void DeleteUser(User user)
        {
            userRepository.Delete(user);
        }

        public void EnterRoom(int roomId, string userName)
        {
            User user = GetUserByName(userName);
            EnterRoom(roomId, user);
        }

        public void EnterRoom(int roomId, User user)
        {
            if (user != null)
            {
                user.ChatRoomId = roomId;
                userRepository.Update(user);
            }
        }

        public void LeaveRoom(User user)
        {
            if (user != null)
            {
                user.ChatRoomId = null;
                userRepository.Update(user);
            }
        }

        public void LeaveRoom(string userName)
        {
            User user = GetUserByName(userName);
            LeaveRoom(user);
        }

        public void DeleteTemporaryUsers(DateTime now, int deltaMinutes)
        {
            List<User> usersToDelete = userRepository
                .GetMany(user => user.IsGuest == true).ToList();

            foreach (User user in usersToDelete)
            {
                if (((TimeSpan)(now - user.LastActivityTime.Value)).TotalMinutes > deltaMinutes)
                {
                    DeleteUser(user);
                }
            }
        }

        public void AssignConnection(string username, string connectionId)
        {
            User user = GetUserByName(username);
            user.ConnectionIdInHub = connectionId;
            userRepository.Update(user);
        }

        public void RegisterUser(User user)
        {
            user.IsGuest = false;
            user.IsOnline = true;
            AddUser(user);
        }
    }
}
