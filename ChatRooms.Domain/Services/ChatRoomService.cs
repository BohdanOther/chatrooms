﻿using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.Domain.Services
{
    public interface IChatRoomService
    {
        void AddRoom(ChatRoom room);
        List<ChatRoom> GetAllChatRooms();
        ChatRoom GetRoomByName(string name);
        ChatRoom GetRoomById(int id);
        void DeleteTemporaryRooms(DateTime now, int deltaMinutes);
    }

    public class ChatRoomService : IChatRoomService
    {
        IGenericRepository<ChatRoom> chatRoomRepository;
        IGenericRepository<User> userRepository;

        public ChatRoomService(IGenericRepository<ChatRoom> chatRoomRepository, IGenericRepository<User> userRepository)
        {
            this.chatRoomRepository = chatRoomRepository;
            this.userRepository = userRepository;
        }

        public List<ChatRoom> GetAllChatRooms()
        {
            List<ChatRoom> rooms = chatRoomRepository.GetAll().ToList();
            return rooms;
        }

        public ChatRoom GetRoomById(int id)
        {
            return chatRoomRepository.GetById(id);
        }

        public void DeleteTemporaryRooms(DateTime now, int deltaMinutes)
        {
            List<ChatRoom> roomsToDelete = GetAllChatRooms();

            foreach (ChatRoom room in roomsToDelete)
            {
                if (((TimeSpan)(now - room.LastActivityTime.Value)).TotalMinutes > deltaMinutes)
                {
                    foreach (User user in room.Users)
                    {
                        user.ChatRoomId = null;
                        userRepository.Update(user);
                    }
                    chatRoomRepository.Delete(room);
                }
            }
        }

        public ChatRoom GetRoomByName(string name)
        {
            return chatRoomRepository.Get(room => room.Name == name);
        }
        public void AddRoom(ChatRoom room)
        {
            chatRoomRepository.Add(room);
        }

    }
}
