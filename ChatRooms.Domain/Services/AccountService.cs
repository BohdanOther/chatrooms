﻿using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.Domain.Services
{
    public interface IAccountService
    {
        bool Login(User user);

        void Logout(User user);
    }

    public class AccountService : IAccountService
    {
        private IGenericRepository<User> userRepository;

        public AccountService(IGenericRepository<User> userRepository)
        {
            this.userRepository = userRepository;
        }

        public bool Login(User userAttempt)
        {
            User _user = null;
            bool loginSucceeded = false;

            if (userAttempt.IsGuest)
            {
                _user = userRepository.Get(u => u.Name == userAttempt.Name);

                if (!(_user != null))
                {
                    _user = userAttempt;
                    _user.IsOnline = true;
                    _user.LastActivityTime = DateTime.Now;
                    userRepository.Add(_user);
                    loginSucceeded = true;
                }
            }
            else
            {
                _user = userRepository.Get(u => u.Name == userAttempt.Name && u.Password == userAttempt.Password);
                if (_user != null)
                {
                    _user.IsOnline = true;
                    _user.LastActivityTime = DateTime.Now;
                    userRepository.Update(_user);
                    loginSucceeded = true;
                }
            }

            return loginSucceeded;
        }


        public void Logout(User user)
        {
            if (user != null)
            {
                if (user.IsGuest)
                {
                    userRepository.Delete(user);
                }
                else
                {
                    user.IsOnline = false;
                    user.LastActivityTime = DateTime.Now;
                    userRepository.Update(user);
                }
            }
        }
    }
}
