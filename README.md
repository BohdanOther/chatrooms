### What is this ###

ChatRooms is basic chat application, written as a .NET course year project.

## Features

* You can chat as guest or signed user, create or join chat rooms.

* As room owner you can kick dummy members from your chat.

* Old rooms and users are automaticly deleted after spesific period of no activity.
 
* Hey, by the way, you can write [LaTeX](https://uk.wikipedia.org/wiki/LaTeX) formulas right in chat messages!

* Chat is localized into English, Italian, Russian and Ukrainian.

## Build
- Ensure you have MS Sql local db, .NET Framework 4.5 and ISS Express installed

- Uncomment line of code below to initialize db with dummy data
```java
public class ChatRoomsContext : DbContext
    {
        public ChatRoomsContext()
            : base("ChatRooms")
        {
            ///Database.SetInitializer<ChatRoomsContext>(new ChatRoomsDbInitializer());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ChatRoom> ChatRooms { get; set; }
    }
```
- or just run migrations to generate db schema

---

## Technologies used
- ASP.NET MVC
- SignalR for chat hub
- [FluentScheduler](https://github.com/fluentscheduler/FluentScheduler) to delete unused chat rooms/users from database
- [MathJax](https://www.mathjax.org/) for LaTeX rendering
- Ninject as DI container
- [Materialize](http://materializecss.com/) for frontend
- AutoMapper for convenient DomainModel-to-ViewModel conversion
- Moq for unit tests ( test coverage more then 70 % )