﻿using System;
using ChatRooms.WebApp.ViewModels;
using Moq;
using ChatRooms.Models;
using ChatRooms.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChatRooms.WebApp.Controllers;
using System.Web.Mvc;
using AutoMapper;

namespace ChatRooms.Tests.WebAppTests
{
    [TestClass]
    public class AccountControllerTests
    {
        [TestMethod]
        public void LoginGuestTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(m => m.Login(It.IsAny<User>())).Returns(true);

            Mock<IUserService> userServiceMock = new Mock<IUserService>();

            Mock<IAuthentication> authMock = new Mock<IAuthentication>();
            authMock.Setup(m => m.DoAuthentication(It.IsAny<string>(), It.IsAny<bool>()));

            LoginViewModel userGuest = new LoginViewModel() {UserId = 0 , Name = "userName"};

            AccountController controller = new AccountController(accountServiceMock.Object, userServiceMock.Object, authMock.Object);
            Mapper.CreateMap<LoginViewModel, User>();
            //Act
            var result = (RedirectToRouteResult)controller.Login(userGuest);

            //Assert
            Assert.AreEqual("Browse", result.RouteValues["action"]);
        }
        [TestMethod]
        public void LoginGuestTestController_ControllerSuchUserExist()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(m => m.Login(It.IsAny<User>())).Returns(false);

            Mock<IUserService> userServiceMock = new Mock<IUserService>();

            Mock<IAuthentication> authMock = new Mock<IAuthentication>();
            authMock.Setup(m => m.DoAuthentication(It.IsAny<string>(), It.IsAny<bool>()));

            LoginViewModel userGuest = new LoginViewModel() { UserId = 0, Name = "userName" };

            AccountController controller = new AccountController(accountServiceMock.Object, userServiceMock.Object, authMock.Object);
            Mapper.CreateMap<LoginViewModel, User>();
            //Act
            var result = controller.Login(userGuest) as ViewResult;

            //Assert
            Assert.AreEqual("Login", result.ViewName);
        }
        [TestMethod]
        public void LoginUserTestController_CorrectPass()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(m => m.Login(It.IsAny<User>())).Returns(true);

            Mock<IUserService> userServiceMock = new Mock<IUserService>();

            Mock<IAuthentication> authMock = new Mock<IAuthentication>();
            authMock.Setup(m => m.DoAuthentication(It.IsAny<string>(), It.IsAny<bool>()));

            LoginViewModel userGuest = new LoginViewModel() { UserId = 0, Name = "userName", Password = "pass" };

            AccountController controller = new AccountController(accountServiceMock.Object, userServiceMock.Object, authMock.Object);
            Mapper.CreateMap<LoginViewModel, User>();
            //Act
            var result = (RedirectToRouteResult)controller.Login(userGuest);

            //Assert
            Assert.AreEqual("Browse", result.RouteValues["action"]);
        }
        [TestMethod]
        public void LoginUserTestController_IncorrectPass()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(m => m.Login(It.IsAny<User>())).Returns(false);

            Mock<IUserService> userServiceMock = new Mock<IUserService>();

            Mock<IAuthentication> authMock = new Mock<IAuthentication>();
            authMock.Setup(m => m.DoAuthentication(It.IsAny<string>(), It.IsAny<bool>()));

            LoginViewModel userGuest = new LoginViewModel() { UserId = 0, Name = "userName", Password = "pass" };

            AccountController controller = new AccountController(accountServiceMock.Object, userServiceMock.Object, authMock.Object);
            Mapper.CreateMap<LoginViewModel, User>();
            //Act
            var result = controller.Login(userGuest) as ViewResult;

            //Assert
            Assert.AreEqual("Login", result.ViewName);
        }
        [TestMethod]
        public void LogOutGuestTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(m => m.GetUserByName(It.IsAny<string>())).Returns(()=> new User());
            Mock<IAuthentication> authMock = new Mock<IAuthentication>();
            
            LoginViewModel userGuest = new LoginViewModel() { UserId = 0, Name = "userName", Password = "pass" };

            AccountController controller = new AccountController(accountServiceMock.Object, userServiceMock.Object, authMock.Object);
            Mapper.CreateMap<LoginViewModel, User>();
            //Act
            //var result = (RedirectToRouteResult)controller.Logout();

            //Assert
            //Assert.AreEqual("Login", result.RouteValues["action"]);
        }
        
    }
}
