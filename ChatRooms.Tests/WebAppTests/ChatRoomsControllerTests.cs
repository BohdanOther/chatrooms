﻿using System;
using ChatRooms.WebApp.ViewModels;
using Moq;
using ChatRooms.Models;
using ChatRooms.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChatRooms.WebApp.Controllers;
using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;

namespace ChatRooms.Tests.WebAppTests
{
    [TestClass]
    public class ChatRoomsControllerTests
    {
        [TestMethod]
        public void AboutTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            Mock<IChatRoomService> chatRoomServiceMock = new Mock<IChatRoomService>();

            ChatRoomsController controller = new ChatRoomsController(chatRoomServiceMock.Object, userServiceMock.Object);
            //Act
            var result = controller.About() as ViewResult;
            
            //Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void BrowseTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            Mock<IChatRoomService> chatRoomServiceMock = new Mock<IChatRoomService>();
            chatRoomServiceMock.Setup(m => m.GetAllChatRooms()).Returns(new List<ChatRoom>());

            ChatRoomsController controller = new ChatRoomsController(chatRoomServiceMock.Object, userServiceMock.Object);
            //Act
            var result = controller.Browse() as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void RoomTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            Mock<IChatRoomService> chatRoomServiceMock = new Mock<IChatRoomService>();
            ChatRoom expectedRoom = new ChatRoom();
            chatRoomServiceMock.Setup(m => m.GetRoomById(It.IsAny<int>())).Returns(expectedRoom);

            ChatRoomsController controller = new ChatRoomsController(chatRoomServiceMock.Object, userServiceMock.Object);
            //Act
            //var result = controller.Room(0) as ViewResult;
            //ChatRoom actualRoom = (ChatRoom)result.ViewData.Model;
            //Assert
            //Assert.AreSame(expectedRoom, actualRoom);
        }
        [TestMethod]
        public void GetAllRoomsLookupTest_Controller()
        {
            //Arrange
            Mock<IAccountService> accountServiceMock = new Mock<IAccountService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            Mock<IChatRoomService> chatRoomServiceMock = new Mock<IChatRoomService>();
            chatRoomServiceMock.Setup(m => m.GetAllChatRooms()).Returns(new List<ChatRoom>());

            ChatRoomsController controller = new ChatRoomsController(chatRoomServiceMock.Object, userServiceMock.Object);
            //Act
            var result = controller.GetAllRoomsLookup();
         
            //Assert
            Assert.IsNotNull(result);
        }
    }
}
