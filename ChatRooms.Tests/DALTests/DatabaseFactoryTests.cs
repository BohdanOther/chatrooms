﻿using System;
using ChatRooms.DAL.Infrastructure.DatabaseFactory;
using ChatRooms.DAL.Infrastructure;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChatRooms.Tests
{
    [TestClass]
    public class DatabaseFactoryTests
    {
        [TestMethod]
        public void GetContextTest()
        {
            //Arrange
            DefaultDatabaseFactory factory = new DefaultDatabaseFactory();
            //Act
            var context = factory.Get();
            //Assert
            Assert.IsInstanceOfType(context, typeof(ChatRoomsContext));
        }
    }
}
