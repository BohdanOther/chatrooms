﻿using System;
using Moq;
using ChatRooms.Domain.Services;
using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace ChatRooms.Tests.DomainTests
{
    [TestClass]
    public class UserServiceTests
    {
        [TestMethod]
        public void GetAllUsersInRoomTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();          
            List<User> usersInRoom = new List<User>(){
                new User(),
                new User(),
                new User()
            };

            repositoryMock.Setup(m => m.GetMany(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => usersInRoom);

            UserService service = new UserService(repositoryMock.Object);
            int fakeRoomId = 12;
            //Act
            List<User> usersReturned = service.GetAllUsersInRoom(fakeRoomId);
            //Assert
            Assert.AreSame(usersInRoom[0],usersReturned[0]);
        }
        [TestMethod]
        public void GetUserByNameTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User user = new User { Name = "userName"};

            repositoryMock.Setup(m => m.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => user);

            UserService service = new UserService(repositoryMock.Object);
            //Act
            User returnedUser = service.GetUserByName(user.Name);
            //Assert
            Assert.AreSame(user, returnedUser);
        }
        [TestMethod]
        public void AddUserTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            List<User> fakeRepository = new List<User>();
            repositoryMock.Setup(m => m.Add(It.IsAny<User>())).Callback((User userToAdd) => { fakeRepository.Add(userToAdd);});

            UserService service = new UserService(repositoryMock.Object);
            User user = new User();
            //Act
            service.AddUser(user);
            //Assert
            Assert.AreSame(user, fakeRepository[0]);
        }
        [TestMethod]
        public void DeleteUserTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User user = new User();
            List<User> fakeRepository = new List<User>(){
                user
            };
            repositoryMock.Setup(m => m.Delete(It.IsAny<User>())).Callback((User userToAdd) => { fakeRepository.Remove(user); });

            UserService service = new UserService(repositoryMock.Object);
            
            //Act
            service.DeleteUser(user);
            //Assert
            Assert.AreEqual(fakeRepository.Count,0);
        }
        [TestMethod]
        public void EnterRoomTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            int oldRoomId = 0;
            int newRoomId = 1;
            User user = new User { Name = "userName", ChatRoomId = oldRoomId };
            repositoryMock.Setup(m => m.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => user);
            repositoryMock.Setup(m => m.Update(It.IsAny<User>())).Callback((User userToUpdate) => { user.ChatRoomId = userToUpdate.ChatRoomId; });

            UserService service = new UserService(repositoryMock.Object);

            //Act
            service.EnterRoom(newRoomId,user.Name);
            //Assert
            Assert.AreEqual(newRoomId, user.ChatRoomId);
        }
        [TestMethod]
        public void DeleteTemporaryUsersTest_ConsistanUsers()
        {
            //Arrange 

            //Mocking users repository
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            DateTime activityTime = new DateTime(2000, 1, 1, 0, 0, 0);
            User consistantUser = new User(){
                IsGuest = false,
                LastActivityTime = activityTime
            };
            List<User> users = new List<User>()
            {
               consistantUser
            };
            repositoryMockUsers.Setup(m => m.GetMany(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => new List<User>());
            repositoryMockUsers.Setup(m => m.Delete(It.IsAny<User>())).Callback((User userToDelete) => { users.Remove(userToDelete); });
           
            UserService service = new UserService(repositoryMockUsers.Object);
            //Act
            service.DeleteTemporaryUsers(new DateTime(2000, 1, 1, 3, 0, 0),30);
            //Assert
            Assert.AreEqual(1, users.Count);         
        }
        [TestMethod]
        public void DeleteTemporaryUsersTest_LongAgoLastActivity()
        {
            //Arrange 

            //Mocking users repository
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            DateTime activityTime = new DateTime(2000, 1, 1, 0, 0, 0);
            User guestUser = new User()
            {
                IsGuest = true,
                LastActivityTime = activityTime
            };
            List<User> users = new List<User>()
            {
               guestUser
            };
            repositoryMockUsers.Setup(m => m.GetMany(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => users);
            repositoryMockUsers.Setup(m => m.Delete(It.IsAny<User>())).Callback((User userToDelete) => { users.Remove(userToDelete); });

            UserService service = new UserService(repositoryMockUsers.Object);
            //Act
            service.DeleteTemporaryUsers(new DateTime(2000, 1, 1, 4, 0, 0),30);
            //Assert
            Assert.AreEqual(0, users.Count);
        }
        [TestMethod]
        public void DeleteTemporaryUsersTest_JustLastActivity()
        {
            //Arrange 

            //Mocking users repository
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            DateTime activityTime = new DateTime(2000, 1, 1, 0, 0, 0);
            User guestUser = new User()
            {
                IsGuest = true,
                LastActivityTime = activityTime
            };
            List<User> users = new List<User>()
            {
               guestUser
            };
            repositoryMockUsers.Setup(m => m.GetMany(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => users);
            repositoryMockUsers.Setup(m => m.Delete(It.IsAny<User>())).Callback((User userToDelete) => { users.Remove(userToDelete); });

            UserService service = new UserService(repositoryMockUsers.Object);
            //Act
            service.DeleteTemporaryUsers(new DateTime(2000, 1, 1, 0, 20, 0),30);
            //Assert
            Assert.AreEqual(1, users.Count);
        }

    }
}
