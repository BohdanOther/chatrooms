﻿using System;
using Moq;
using ChatRooms.Domain.Services;
using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Diagnostics;

namespace ChatRooms.Tests.DomainTests
{
    [TestClass]
    public class ChatRoomServiceTests
    {
        [TestMethod]
        public void GetAllChatRoomsTest()
        {
            //Arrange 
            Mock<IGenericRepository<ChatRoom>> repositoryMockRooms = new Mock<IGenericRepository<ChatRoom>>();
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            List<ChatRoom> rooms = new List<ChatRoom>(){
                new ChatRoom(),
                new ChatRoom()
            };
            repositoryMockRooms.Setup(m => m.GetAll()).Returns(() => rooms);

            ChatRoomService service = new ChatRoomService(repositoryMockRooms.Object,repositoryMockUsers.Object);
            //Act
            List<ChatRoom> returnedRooms = service.GetAllChatRooms();
            //Assert
            Assert.AreSame(rooms[0], returnedRooms[0]);
        }
        [TestMethod]
        public void GetRoomByIdTest()
        {
            //Arrange 
            Mock<IGenericRepository<ChatRoom>> repositoryMockRooms = new Mock<IGenericRepository<ChatRoom>>();
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            int Id = 1;
            ChatRoom room = new ChatRoom() { ChatRoomId = Id };
            repositoryMockRooms.Setup(m => m.GetById(Id)).Returns(() => room);
            ChatRoomService service = new ChatRoomService(repositoryMockRooms.Object,repositoryMockUsers.Object);
            //Act
            ChatRoom returnedRoom = service.GetRoomById(Id);
            //Assert
            Assert.AreSame(room, returnedRoom);
        }
        [TestMethod]
        public void GetRoomByNameTest()
        {
            //Arrange 
            Mock<IGenericRepository<ChatRoom>> repositoryMockRooms = new Mock<IGenericRepository<ChatRoom>>();
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();

            string roomName = "roomName";
            ChatRoom room = new ChatRoom() { Name = roomName };
            repositoryMockRooms.Setup(m => m.Get(It.IsAny<Expression<Func<ChatRoom,bool>>>())).Returns(() => room);

            ChatRoomService service = new ChatRoomService(repositoryMockRooms.Object, repositoryMockUsers.Object);
            //Act
            ChatRoom returnedRoom = service.GetRoomByName(roomName);
            //Assert
            Assert.AreSame(room, returnedRoom);
        }
        [TestMethod]
        public void GetRoomByNameTest_NoSuchRoom()
        {
            //Arrange 
            Mock<IGenericRepository<ChatRoom>> repositoryMockRooms = new Mock<IGenericRepository<ChatRoom>>();
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();

            string roomName = "roomName";
            ChatRoom room = new ChatRoom() { Name = roomName };
            repositoryMockRooms.Setup(m => m.Get(It.IsAny<Expression<Func<ChatRoom, bool>>>())).Returns(() => null);

            ChatRoomService service = new ChatRoomService(repositoryMockRooms.Object, repositoryMockUsers.Object);
            //Act
            ChatRoom returnedRoom = service.GetRoomByName("differentName");
            //Assert
            Assert.IsNull(returnedRoom);
        }
        [TestMethod]
        public void AddRoomTest()
        {
            //Arrange 
            Mock<IGenericRepository<ChatRoom>> repositoryMockRomms = new Mock<IGenericRepository<ChatRoom>>();
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();

            List<ChatRoom> rooms = new List<ChatRoom>();
            repositoryMockRomms.Setup(m => m.Add(It.IsAny<ChatRoom>())).Callback((ChatRoom roomToAdd) => rooms.Add(roomToAdd));

            ChatRoomService service = new ChatRoomService(repositoryMockRomms.Object, repositoryMockUsers.Object);
            ChatRoom room = new ChatRoom();
            //Act
            service.AddRoom(room);
            //Assert
            Assert.AreSame(rooms[0],room);
        }
        [TestMethod]
        public void DeleteUselessRoomsTest()
        {
            //Arrange 

            //Mocking users repository
            Mock<IGenericRepository<User>> repositoryMockUsers = new Mock<IGenericRepository<User>>();
            List<User> users = new List<User>()
            {
                new User(){ChatRoomId  = 1}
            };
            repositoryMockUsers.Setup(m => m.Update(It.IsAny<User>())).Callback(() => { users[0].ChatRoomId = null; });

            //Mocking rooms repository
            Mock<IGenericRepository<ChatRoom>> repositoryMockRooms = new Mock<IGenericRepository<ChatRoom>>();
            ChatRoom uselessRoom = new ChatRoom();
            uselessRoom.LastActivityTime = new DateTime(2000, 1, 1, 0, 0, 0);
            uselessRoom.Users = users;
            ChatRoom notUselessRoom = new ChatRoom();
            notUselessRoom.LastActivityTime = new DateTime(2000, 1, 1, 2, 0, 0);
            List<ChatRoom> rooms = new List<ChatRoom>()
            {
                uselessRoom,
                notUselessRoom
            };
            repositoryMockRooms.Setup(m => m.GetAll()).Returns(() => rooms);
            repositoryMockRooms.Setup(m => m.Delete(It.IsAny<ChatRoom>())).Callback((ChatRoom roomToDelete) => { rooms.Remove(roomToDelete); });
            
            ChatRoomService service = new ChatRoomService(repositoryMockRooms.Object, repositoryMockUsers.Object);
            //Act
            service.DeleteTemporaryRooms(new DateTime(2000, 1, 1, 4, 0, 0),180);
            //Assert
            Debug.WriteLine(rooms[0].LastActivityTime);
            Assert.AreEqual(1, rooms.Count);
            Assert.AreSame(notUselessRoom, rooms[0]);
        }


    }
}
