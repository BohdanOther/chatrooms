﻿using System;
using Moq;
using ChatRooms.Domain.Services;
using ChatRooms.DAL.Infrastructure.Repository;
using ChatRooms.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;

namespace ChatRooms.Tests.DomainTests
{
    class Test { };
    [TestClass]
    public class AccountServiceTests
    {
        [TestMethod]
        public void LoginGuestTest_NoSuchUserInDB()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", IsGuest = true };            
            repositoryMock.Setup(m => m.Get(It.IsAny <Expression<Func<User, bool>>>())).Returns(() => null);

            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            bool result = service.Login(userAttempt);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void LoginGuestTest_UserAlreadyExists()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", IsGuest = true };
            repositoryMock.Setup(m => m.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => new User() { Name = userAttempt.Name });
           
            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            bool result = service.Login(userAttempt);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void LoginExistingUserTest_CorrectPass()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", Password = "userPass", IsGuest = false };
            repositoryMock.Setup(m => m.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => new User() { Name = userAttempt.Name, Password = userAttempt.Password });

            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            bool result = service.Login(userAttempt);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void LoginExistingUserTest_IncorrectPass()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", Password = "userPass", IsGuest = false };
            repositoryMock.Setup(m => m.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(() => null);
            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            bool result = service.Login(userAttempt);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void LogoutGuestTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", IsGuest = true };
            repositoryMock.Setup(m => m.Delete(It.IsAny<User>())).Callback((User userToDelete) => { userAttempt = null; });

            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            service.Logout(userAttempt);
            //Assert
            Assert.IsNull(userAttempt);

        }
        [TestMethod]
        public void LogoutExistingUserTest()
        {
            //Arrange 
            Mock<IGenericRepository<User>> repositoryMock = new Mock<IGenericRepository<User>>();
            User userAttempt = new User { Name = "userName", Password = "userPass", IsGuest = false };
            repositoryMock.Setup(m => m.Update(It.IsAny<User>())).Callback((User userUpdated) => { userAttempt = userUpdated; });

            AccountService service = new AccountService(repositoryMock.Object);
            //Act
            service.Logout(userAttempt);
            //Assert
            Assert.IsFalse(userAttempt.IsOnline);
        }
        

    }
}
