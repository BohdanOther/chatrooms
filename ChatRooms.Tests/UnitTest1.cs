﻿using System;
using ChatRooms.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChatRooms.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // arrange        
            decimal total = 200;
            // act
           
            // assert
            Assert.AreEqual(total, 10);
        }
    }
}
