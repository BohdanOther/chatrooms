﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ChatRooms.WebApp.Controllers
{
    public interface IAuthentication
    {
        void DoAuthentication(string userName, bool remember);
        void SignOut();
        
    }
    public class DefaultAuthenticator: IAuthentication
    {
        public  void DoAuthentication(string userName, bool remember)
        {
            FormsAuthentication.SetAuthCookie(userName, remember);
        }
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
