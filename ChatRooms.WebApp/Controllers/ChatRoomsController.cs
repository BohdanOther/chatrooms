﻿using ChatRooms.Domain.Services;
using ChatRooms.Models;
using ChatRooms.WebApp.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace ChatRooms.WebApp.Controllers
{
    public class ChatRoomsController : Controller
    {
        IChatRoomService chatRoomService;
        IUserService userService;

        public ChatRoomsController(IChatRoomService chatRoomService, IUserService userService)
        {
            this.chatRoomService = chatRoomService;
            this.userService = userService;
        }

        // GET: ChatRooms
        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        // GET: ChatRooms/Browse
        public ActionResult Browse()
        {
            // Get 9 most popular rooms
            List<ChatRoom> rooms = chatRoomService
                .GetAllChatRooms()
                .OrderByDescending(room => room.Users.Count)
                .Take(9)
                .ToList();

            return View(new BrowseRoomViewModel() { Rooms = rooms, CreateRoom = new CreateRoomViewModel() });
        }

        // GET: ChatRooms/Room?roomId=1
        [HttpGet]
        public ActionResult Room(int roomId)
        {
            userService.EnterRoom(roomId, User.Identity.Name);

            ChatRoom room = chatRoomService.GetRoomById(roomId);
            return View(room);
        }

        [HttpGet]
        public ActionResult Leave(string user)
        {
            userService.LeaveRoom(user);
            return RedirectToAction("Browse");
        }

        // GET: /ChatRooms/Create/
        [HttpPost]
        public ActionResult Create(CreateRoomViewModel newRoom)
        {
            bool roomExists = chatRoomService.GetRoomByName(newRoom.Name) != null;

            if (roomExists || !ModelState.IsValid)
            {
                ModelState.AddModelError("", "Sorry, this room already exists");

                ViewData["ModelIsValid"] = "false";

                List<ChatRoom> rooms = chatRoomService
                .GetAllChatRooms()
                .OrderByDescending(room => room.Users.Count)
                .Take(9)
                .ToList();

                return View("Browse", new BrowseRoomViewModel() { Rooms = rooms, CreateRoom = new CreateRoomViewModel() });
            }
            else
            {
                var room = AutoMapper.Mapper.Map<ChatRoom>(newRoom);
                room.OwnerName = User.Identity.Name;
                chatRoomService.AddRoom(room);

                return RedirectToAction("Room", "ChatRooms", new { roomId = room.ChatRoomId });
            }
        }

        public JsonResult GetAllRoomsLookup()
        {
            List<ChatRoom> rooms = chatRoomService.GetAllChatRooms();

            var list = rooms.Select(i => new { i.ChatRoomId, i.Name, rooms.Count }).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Help()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Kicked()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Users(int roomId)
        {
            ChatRoom room = chatRoomService.GetRoomById(roomId);
            return PartialView(room);
        }
    }
}
