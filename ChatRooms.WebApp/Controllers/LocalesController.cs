using System;
using System.Globalization;
using System.Web.Mvc;

namespace ChatRooms.WebApp.Controllers
{
    public class LocalesController : Controller
    {
        [AllowAnonymous]
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}