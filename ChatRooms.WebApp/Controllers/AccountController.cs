﻿using ChatRooms.Domain.Services;
using ChatRooms.Models;
using ChatRooms.WebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ChatRooms.WebApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        IAccountService accountService;
        IUserService userService;
        IAuthentication auth;

        public AccountController(IAccountService accountService, IUserService userService, IAuthentication auth)
        {
            this.accountService = accountService;
            this.userService = userService;
            this.auth = auth;
        }

        // GET: Account/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: Account/Login
        [HttpPost]
        public ActionResult Login(LoginViewModel userViewModel)
        {
            var user = AutoMapper.Mapper.Map<User>(userViewModel);

            bool userIsGuest = userViewModel.Password != null ? false : true;

            user.IsGuest = userIsGuest;

            bool loginSecceded = accountService.Login(user);

            if (loginSecceded)
            {
                auth.DoAuthentication(user.Name, true);
                return RedirectToAction("Browse", "ChatRooms");
            }
            else
            {
                string errorMessage = userIsGuest ?
                    "This user already exists" :
                    "Wrong login data";

                ModelState.AddModelError("", errorMessage);
                return View("Login");
            }
        }

        // GET: Account/Logout
        public ActionResult Logout()
        {
            User user = userService.GetUserByName(User.Identity.Name);

            accountService.Logout(user);

            auth.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterView newUser)
        {
            var user = AutoMapper.Mapper.Map<User>(newUser);
            userService.RegisterUser(user);
            return RedirectToAction("Browse", "ChatRooms");
        }
    }
}