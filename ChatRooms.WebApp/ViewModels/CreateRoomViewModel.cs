﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChatRooms.WebApp.ViewModels
{
    public class CreateRoomViewModel
    {
        [Required]
        [MinLength(3, ErrorMessage = "Minimum lenght for room name is 3")]
        [MaxLength(15, ErrorMessage = "Please, use shorter room name")]
        public string Name { get; set; }
    }
}