﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatRooms.WebApp.ViewModels
{
    public class RegisterView
    {
        [Required]
        [MinLength(2, ErrorMessage = "Minimum length is 2")]
        public string Name { get; set; }

        [Required]
        [MinLength(4, ErrorMessage = "Minimum length is 4")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Password don't match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}