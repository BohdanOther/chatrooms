﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRooms.WebApp.ViewModels
{
    public class BrowseRoomViewModel
    {
        public List<ChatRooms.Models.ChatRoom> Rooms { get; set; }
        public CreateRoomViewModel CreateRoom { get; set; }
    }
}