﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatRooms.WebApp.ViewModels
{
    public class LoginViewModel
    {
        [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [MinLength(2, ErrorMessage = "Minimum name length is 2")]
        public string Name { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}