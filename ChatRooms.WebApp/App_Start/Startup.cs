﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;
using ChatRooms.WebApp.Hubs;
using ChatRooms.Domain.Services;
using Ninject;

[assembly: OwinStartup(typeof(ChatRooms.WebApp.App_Start.Startup))]
namespace ChatRooms.WebApp.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var resolver = new NinjectSignalRDependencyResolver(NinjectWebCommon.CreateKernel());
            var config = new HubConfiguration();
            config.Resolver = resolver;
            app.MapSignalR(config);
        }
    }
}
