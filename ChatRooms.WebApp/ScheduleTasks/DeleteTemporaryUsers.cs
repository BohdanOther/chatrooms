﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatRooms.Domain.Services;
using ChatRooms.Models;
using FluentScheduler;
using System.Diagnostics;
using Ninject;

namespace ChatRooms.WebApp.ScheduleTasks
{
    public class DeleteTemporaryUsers : ITask
    {
        private IUserService _service;

        [Inject]
        public IUserService Service
        {
            get
            {
                return _service;
            }
            set
            {
                _service = value;
            }
        }
        
        public void Execute()
        {
            Service.DeleteTemporaryUsers(DateTime.Now, 30);
            //Debug.WriteLine("Temporary users deleted at: " + DateTime.Now);                   
        }
    }
}