﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatRooms.Domain.Services;
using ChatRooms.Models;
using FluentScheduler;
using System.Diagnostics;
using Ninject;

namespace ChatRooms.WebApp.ScheduleTasks
{
    public class DeleteUselessRooms : ITask
    {
        private IChatRoomService _service;

        [Inject]
        public IChatRoomService Service
        {
            get
            {
                return _service;
            }
            set
            {
                _service = value;
            }
        }
        public void Execute()
        {
            Service.DeleteTemporaryRooms(DateTime.Now, 180);
            //Console.WriteLine("Temporary rooms deleted at: " + DateTime.Now);
        }
    }
}