﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;
using ChatRooms.Domain.Services;
using System.Web.Mvc;

namespace ChatRooms.WebApp.ScheduleTasks
{
    public class MyTaskFactory: ITaskFactory
    {    
        public ITask GetTaskInstance<T>() where T : ITask
        {
            // If you're creating your own ITaskFactory, typically your DI framework of choice would take care of this
            //Console.WriteLine("Creating task: " + typeof(T));
            return DependencyResolver.Current.GetService<T>();
        }
    }
}