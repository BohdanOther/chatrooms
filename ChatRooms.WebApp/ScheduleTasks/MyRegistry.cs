﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;

namespace ChatRooms.WebApp.ScheduleTasks
{
    public class MyRegistry: Registry
    {
        public MyRegistry()
        {
            // Schedule an ITask to run at an interval
            Schedule<DeleteTemporaryUsers>().ToRunEvery(1).Hours();
            Schedule<DeleteUselessRooms>().ToRunEvery(1).Hours();
        }
    }
}