﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using ChatRooms.Domain.Services;
using ChatRooms.Models;
using Ninject;

namespace ChatRooms.WebApp.Hubs
{
    public class ChatHub : Hub
    {
        private IUserService userService;
        private IChatRoomService chatRoomService;

        public ChatHub(IUserService userService, IChatRoomService chatRoomService)
        {
            this.userService = userService;
            this.chatRoomService = chatRoomService;
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            User connectedUser = userService.GetUserByName(Context.User.Identity.Name);
            userService.AssignConnection(Context.User.Identity.Name, Context.ConnectionId);
            if (connectedUser != null && connectedUser.ChatRoomId != null)
            {
                int roomId = (int)connectedUser.ChatRoomId;
                ChatRoom room = chatRoomService.GetRoomById(roomId);
                Groups.Add(Context.ConnectionId, room.Name);
                Clients.Group(room.Name).onConnected(Context.User.Identity.Name);
                Clients.OthersInGroup(room.Name).reloadUsers(roomId);
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            User connectedUser = userService.GetUserByName(Context.User.Identity.Name);
            userService.AssignConnection(Context.User.Identity.Name, "");
            if (connectedUser != null && connectedUser.ChatRoomId != null)
            {
                ChatRoom room = chatRoomService.GetRoomById(connectedUser.ChatRoom.ChatRoomId);

                Clients.OthersInGroup(room.Name).userLeft(Context.User.Identity.Name);
                Groups.Remove(Context.ConnectionId, room.Name);

                userService.LeaveRoom(connectedUser);
            }
            return base.OnDisconnected(stopCalled);
        }

        public void Send(string room, string user, string message)
        {
            Clients.Group(room).addNewMessageToPage(user, message);
        }

        public void KickUser(string userName, string room)
        {
            User kickedUser = userService.GetUserByName(userName);
            Clients.Client(kickedUser.ConnectionIdInHub).kickUser();
            if (kickedUser != null)
            {
                userService.LeaveRoom(kickedUser);
            }
            Clients.Group(room).userKicked(userName);
        }
    }
}