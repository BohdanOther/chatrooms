﻿$(document).ready(function () {
    $('.parallax').parallax();

    $(".button-collapse").sideNav({
        menuWidth: 300
    });

    $('.modal-trigger').leanModal();

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        belowOrigin: true // Displays dropdown below the button
    }
 );
});