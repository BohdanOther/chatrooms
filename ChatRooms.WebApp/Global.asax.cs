﻿using ChatRooms.WebApp.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FluentScheduler;
using System.Globalization;
using System.Threading;

namespace ChatRooms.WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            TaskManager.TaskFactory = new ScheduleTasks.MyTaskFactory();
            TaskManager.Initialize(new ScheduleTasks.MyRegistry()); 
            GlobalFilters.Filters.Add(new AuthorizeAttribute());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfiguration.Configure();
        }
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {          
            if (HttpContext.Current.Session != null)
            {
                CultureInfo ci = (CultureInfo)this.Session["Culture"];               
                if (ci == null)
                {
                    string langName = "en";
                    if (HttpContext.Current.Request.UserLanguages != null &&
                            HttpContext.Current.Request.UserLanguages.Length != 0)
                    { 
                        langName = HttpContext.Current.Request.UserLanguages[0].Substring(0, 2);
                    }
                    ci = new CultureInfo(langName);
                    this.Session["Culture"] = ci;
                }
                
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
        }
    }
}
