﻿using AutoMapper;
using ChatRooms.Models;
using ChatRooms.WebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRooms.WebApp.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<LoginViewModel, User>();
            Mapper.CreateMap<CreateRoomViewModel, ChatRoom>();
            Mapper.CreateMap<RegisterView, User>();
        }
    }
}