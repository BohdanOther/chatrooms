namespace ChatRooms.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatRooms",
                c => new
                    {
                        ChatRoomId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastActivityTime = c.DateTime(),
                        TimeCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ChatRoomId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                        IsOnline = c.Boolean(nullable: false),
                        IsGuest = c.Boolean(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        ChatRoomId = c.Int(),
                        LastActivityTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.ChatRooms", t => t.ChatRoomId)
                .Index(t => t.ChatRoomId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "ChatRoomId", "dbo.ChatRooms");
            DropIndex("dbo.Users", new[] { "ChatRoomId" });
            DropTable("dbo.Users");
            DropTable("dbo.ChatRooms");
        }
    }
}
