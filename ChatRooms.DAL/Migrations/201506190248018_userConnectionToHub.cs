namespace ChatRooms.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userConnectionToHub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ConnectionIdInHub", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ConnectionIdInHub");
        }
    }
}
