// <auto-generated />
namespace ChatRooms.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class userConnectionToHub : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(userConnectionToHub));
        
        string IMigrationMetadata.Id
        {
            get { return "201506190248018_userConnectionToHub"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
