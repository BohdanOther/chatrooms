namespace ChatRooms.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ChatRooms.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ChatRooms.DAL.Infrastructure.ChatRoomsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ChatRooms.DAL.Infrastructure.ChatRoomsContext context)
        {
            context.ChatRooms.AddOrUpdate(
                p => p.Name,

                new ChatRoom() { Name = "uporoti pacuku", ChatRoomId = 1, OwnerName = "b.other" },
                new ChatRoom() { Name = "niggers here", ChatRoomId = 2, OwnerName = "b.other" },
                new ChatRoom() { Name = "just chat", ChatRoomId = 3, OwnerName = "b.other" },
                new ChatRoom() { Name = "like4like", ChatRoomId = 4, OwnerName = "b.other" },
                new ChatRoom() { Name = "who are over 18", ChatRoomId = 5, OwnerName = "b.other" },
                new ChatRoom() { Name = "Music", ChatRoomId = 6, OwnerName = "b.other" },
                new ChatRoom() { Name = "Films", ChatRoomId = 7, OwnerName = "b.other" },
                new ChatRoom() { Name = "Games", ChatRoomId = 8, OwnerName = "b.other" },
                new ChatRoom() { Name = "TechSupport", ChatRoomId = 9, OwnerName = "b.other" }
           );

            context.Users.AddOrUpdate(
                user => user.Name,

                new User() { Name = "b.other", Password = "12345", ChatRoomId = 1, IsGuest = false },
                new User() { Name = "urion", Password = "love_hapko", ChatRoomId = 2, IsGuest = false },
                new User() { Name = "vasya", Password = "v_rot_ibaw", ChatRoomId = 2, IsGuest = false },
                new User() { Name = "gagarina", Password = "ne_bud'_skotunoju", ChatRoomId = 1, IsGuest = false },
                new User() { Name = "VIP_GRISHA", Password = "SEXSEXSEX", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "pussy", Password = "pussy", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "ramaza", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "igorko", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "romko", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "stefko", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "timurka", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "alla", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "rosalin", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "bigPapik", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "renatka", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "kykyryza", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "roman", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "bella", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "ada", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "mazda1696", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "rafik", Password = "qwerty", ChatRoomId = 7, IsGuest = false },
                new User() { Name = "kurulo", Password = "qwerty", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "olga666", Password = "qwerty", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "kurulo666", Password = "qwerty", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "inga69", Password = "qwerty", ChatRoomId = 5, IsGuest = false },
                new User() { Name = "ololo", Password = "qwerty", ChatRoomId = 3, IsGuest = false },
                new User() { Name = "linda", Password = "qwerty", ChatRoomId = 3, IsGuest = false },
                new User() { Name = "irma", Password = "qwerty", ChatRoomId = 3, IsGuest = false },
                new User() { Name = "kristy", Password = "qwerty", ChatRoomId = 3, IsGuest = false }
             );
        }
    }
}
