namespace ChatRooms.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoomOwner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChatRooms", "OwnerId", c => c.Int(nullable: false));
            AddColumn("dbo.ChatRooms", "OwnerName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChatRooms", "OwnerName");
            DropColumn("dbo.ChatRooms", "OwnerId");
        }
    }
}
