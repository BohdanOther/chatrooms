namespace ChatRooms.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoomOwnerRemoveName : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ChatRooms", "OwnerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChatRooms", "OwnerId", c => c.Int(nullable: false));
        }
    }
}
