﻿using ChatRooms.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.DAL.Infrastructure
{
    public class ChatRoomsContext : DbContext
    {
        public ChatRoomsContext()
            : base("ChatRooms")
        {
            ///Database.SetInitializer<ChatRoomsContext>(new ChatRoomsDbInitializer());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ChatRoom> ChatRooms { get; set; }
    }
}
