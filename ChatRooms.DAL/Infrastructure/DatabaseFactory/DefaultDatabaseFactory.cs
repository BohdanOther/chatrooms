﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.DAL.Infrastructure.DatabaseFactory
{
    public class DefaultDatabaseFactory : IDatabaseFactory
    {
        private ChatRoomsContext dataContext;

        public ChatRoomsContext Get()
        {
            return dataContext ?? (dataContext = new ChatRoomsContext());
        }

        private bool isDisposed;

        ~DefaultDatabaseFactory()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }

        protected void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
