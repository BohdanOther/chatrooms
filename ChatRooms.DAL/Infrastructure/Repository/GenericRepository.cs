﻿using ChatRooms.DAL.Infrastructure.DatabaseFactory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.DAL.Infrastructure.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private ChatRoomsContext dataContext;
        private readonly IDbSet<TEntity> dbset;

        public GenericRepository(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<TEntity>();
        }

        protected IDatabaseFactory DatabaseFactory { get; private set; }

        protected ChatRoomsContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        public virtual void Add(TEntity entity)
        {
            dbset.Add(entity);
            dataContext.SaveChanges();
        }

        public virtual void Update(TEntity entity)
        {
            dataContext.Entry(entity).State = EntityState.Modified;
            dataContext.SaveChanges();
        }

        public virtual void Delete(TEntity entity)
        {
            dbset.Remove(entity);
            dataContext.SaveChanges();
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            IEnumerable<TEntity> objects = dbset.Where<TEntity>(where).AsEnumerable();
            foreach (TEntity obj in objects)
                dbset.Remove(obj);
            dataContext.SaveChanges();
        }

        public virtual TEntity GetById(long id)
        {
            return dbset.Find(id);
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault<TEntity>();
        }

        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return dbset.Where(where).ToList();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbset.ToList();
        }

    }
}
