﻿using ChatRooms.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms.DAL.Infrastructure
{
    class ChatRoomsDbInitializer : DropCreateDatabaseAlways<ChatRoomsContext>
    {
        protected override void Seed(ChatRoomsContext context)
        {
            new List<ChatRoom>()
            {
                new ChatRoom() { Name = "uporoti pacuku", ChatRoomId=1},
                new ChatRoom() { Name = "niggers here", ChatRoomId=2},
                new ChatRoom() { Name = "just chat", ChatRoomId=3},
                new ChatRoom() { Name = "like4like", ChatRoomId=4},
                new ChatRoom() { Name = "who are over 18", ChatRoomId=5},
                new ChatRoom() { Name = "Music", ChatRoomId=6},
                new ChatRoom() { Name = "Films", ChatRoomId=7},
                new ChatRoom() { Name = "Games", ChatRoomId=8},
                new ChatRoom() { Name = "TechSupport", ChatRoomId=9}

            }.ForEach(entity => context.ChatRooms.Add(entity));

            new List<User>()
            {
                new User() { Name = "b.other", Password="12345", ChatRoomId=1},
                new User() { Name = "urion", Password="love_hapko", ChatRoomId=2},
                new User() { Name = "vasya", Password="v_rot_ibaw", ChatRoomId=2},
                new User() { Name = "gagarina", Password = "ne_bud'_skotunoju", ChatRoomId=1},
                new User() { Name = "VIP_GRISHA", Password = "SEXSEXSEX", ChatRoomId=5},
                new User() { Name = "pussy", Password = "pussy", ChatRoomId=5},
                new User() { Name = "ramaza", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "igorko", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "romko", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "stefko", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "timurka", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "alla", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "rosalin", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "bigPapik", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "renatka", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "kykyryza", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "roman", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "bella", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "ada", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "mazda1696", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "rafik", Password = "qwerty", ChatRoomId=7},
                new User() { Name = "kurulo", Password = "qwerty", ChatRoomId=5},
                new User() { Name = "olga666", Password = "qwerty", ChatRoomId=5},
                new User() { Name = "kurulo", Password = "qwerty", ChatRoomId=5},
                new User() { Name = "inga69", Password = "qwerty", ChatRoomId=5},
                new User() { Name = "ololo", Password = "qwerty", ChatRoomId=3},
                new User() { Name = "linda", Password = "qwerty", ChatRoomId=3},
                new User() { Name = "irma", Password = "qwerty", ChatRoomId=3},
                new User() { Name = "kristy", Password = "qwerty", ChatRoomId=3}

            }.ForEach(entity => context.Users.Add(entity));

            base.Seed(context);
        }
    }
}
